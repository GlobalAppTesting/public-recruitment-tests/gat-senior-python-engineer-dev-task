# Python engineer test
We hope this task will give you an opportunity to present your knowledge in
Python language as well as general algorithmic knowledge. However, what is even more important to us is that you have fun with it!

## The task
Here is the task to solve:
We would like to create a map of our website: https://globalapptesting.com.
Can you provide us with a tool for creating the map we are interested in?

Here are some more detailed requirements:
* We want to start from the homepage (https://globalapptesting.com) and find any possible transitions between subpages. You may focus only on using links (<a> tags).
* We are interested in the best possible map representation: could you suggest one?
* We would like to see basic metrics related to our site, namely:
    * What is the distance between the most distant subpages?
    * What is the average number of links coming out of subpages of our website to different websites?
    * What is the average number of internal links on our website?
* We would like to know if there are any dead (pointing to the non-existing pages) links on our site.
* We would like to find the pages which are the most difficult to enter (that have the minimal number of incoming links) when using our webpage.
* We would also like to find pages which are most linked in our website.

Consider different approaches (web technologies, map representations etc.) and pick the best one in your opinion.

## Nice to have (optional, not required):
* We would like to have a text-based UI which allows to ask about the shortest path between two provided subpages (e.g. between https://globalapptesting.com and https://www.globalapptesting.com/customers/facebook). The UI should present a full path between the given subpages.
* We would like to see graphical representation of the subpages of our website and their relations.
* We would like to have the ability to save the generated map and restore it from the file to avoid the need of regenerating the whole map.
* We would like to see what's the average size (in bytes) of a page in our website (HTML only!).

## What is important to us?
First of all: the solution of the problem is important. We would like to see a clear solution to the algorithmic problem, in form of a *github* / *gitlab* repository. If you decide to make it private, please share it with the @radek.janik user. 
We would also like to see your ability to create clean and easy to read Python code.
Make sure that your solution contains a README file and other stuff which speeds up the environment setup.
If there are any other artifacts you want to provide us ? Feel free to add it to your repo.
Remember: this is the engineering task!
